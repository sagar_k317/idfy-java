package com.idfy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.google.gson.*;
import com.idfy.constants.Constants;

public class IDfyClient {

    private String authKey;
    private String baseUrl;
    private String apiVersion;

    public IDfyClient(String authKey) {
        this.authKey = authKey;
        this.baseUrl = Constants.getBaseUrlV2();
        this.apiVersion = "v2";
    }

    public IDfyClient(String authKey, String endPoint) throws IDfyErrors{
        this.authKey = authKey;
        if (endPoint.equals(Constants.getBaseUrlV2())) {
            this.baseUrl = endPoint;
            this.apiVersion = "v2";
        } else {
            throw new IDfyErrors("Invalid endPoint provided. Supported version - https://api.idfystaging.com/v2/tasks");
        }
    }

    public JsonObject postRequest(String taskType, String taskId, Map<String, String> data, String groupId) throws IOException, IDfyErrors {
        Utils utils = new Utils(taskType, taskId, data, groupId);
        String requestBody = new Gson().toJson(utils.validateRequestArguments(this.apiVersion));
        URL url = new URL(this.baseUrl);
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestMethod("POST");
        httpCon.addRequestProperty("Content-Type", Constants.getApplicationJson());
        httpCon.addRequestProperty(Constants.getApiKey(), this.authKey);
        httpCon.setRequestProperty("Content-Length", Integer.toString(requestBody.length()));
        httpCon.getOutputStream().write(requestBody.getBytes(StandardCharsets.UTF_8));
        OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
        out.close();
        BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String jsonString = response.toString().replaceAll("^\\[|]$", "");
        JsonParser jsonParser = new JsonParser();
        JsonElement responseJsonElement = jsonParser.parse(jsonString);
        return responseJsonElement.getAsJsonObject();
    }

    public JsonObject getResponse(Map<String, String> responseQueryArgs) throws IOException, IDfyErrors {
        String queryArgs = "?";
        Utils utils = new Utils();
        for (String key: utils.validateResponseQueryArguments(responseQueryArgs).keySet()) {
            queryArgs = queryArgs.concat(key+"="+responseQueryArgs.get(key)+"&");
        }
        queryArgs = queryArgs.replaceAll("&$", "");
        String urlString = Constants.getBaseUrlV2().concat(queryArgs);
        URL url = new URL(urlString);
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setRequestMethod("GET");
        httpCon.setRequestProperty("Accept-Charset", "UTF-8");
        httpCon.addRequestProperty("Content-Type", Constants.getApplicationJson());
        httpCon.addRequestProperty(Constants.getApiKey(), this.authKey);
        BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
        String line;
        StringBuilder response = new StringBuilder();
        while ((line = in.readLine()) != null) {
            response.append(line);
        }
        in.close();
        String jsonString = response.toString().replaceAll("^\\[", "{\"output\": [");
        jsonString = jsonString.replaceAll("$", "}");
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(jsonString);
        return je.getAsJsonObject();
    }

}
