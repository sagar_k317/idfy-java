package com.idfy.constants;

import java.util.*;

public class Tasks {
    public final Map<String, Map<String, Map<String, Map<String, ArrayList<String>>>>> taskConfig = new HashMap<>();

    public Tasks() {
        final Map<String, Map<String, ArrayList<String>>> dataSchema = new HashMap<>();

        Map<String, ArrayList<String>> panOcr = new HashMap<>();
        panOcr.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        panOcr.put(Constants.getOptionalFields(), new ArrayList<>());
        panOcr.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("pan_ocr", panOcr);

        Map<String, ArrayList<String>> panVerification = new HashMap<>();
        panVerification.put(Constants.getMandateFields(), new ArrayList<>(Arrays.asList("pan_number", "pan_name")));
        panVerification.put(Constants.getOptionalFields(), new ArrayList<>());
        panVerification.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("panVerification", panVerification);

        Map<String, ArrayList<String>> aadhaarOcr = new HashMap<>();
        aadhaarOcr.put(Constants.getMandateFields(), new ArrayList<>(Arrays.asList(Constants.getDocUrl(), "aadhaar_consent")));
        aadhaarOcr.put(Constants.getOptionalFields(), new ArrayList<>());
        aadhaarOcr.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("aadhaarOcr", aadhaarOcr);

        Map<String, ArrayList<String>> chequeOcr = new HashMap<>();
        chequeOcr.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        chequeOcr.put(Constants.getOptionalFields(), new ArrayList<>());
        chequeOcr.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("chequeOcr", chequeOcr);

        Map<String, ArrayList<String>> voterOcr = new HashMap<>();
        voterOcr.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        voterOcr.put(Constants.getOptionalFields(), new ArrayList<>());
        voterOcr.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("voterOcr", voterOcr);

        Map<String, ArrayList<String>> voterVerification = new HashMap<>();
        voterVerification.put(Constants.getMandateFields(), new ArrayList<>(Arrays.asList("voter_number", "voter_name")));
        voterVerification.put(Constants.getOptionalFields(), new ArrayList<>());
        voterVerification.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("voterVerification", voterVerification);

        Map<String, ArrayList<String>> drivingLicenseOcr = new HashMap<>();
        drivingLicenseOcr.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        drivingLicenseOcr.put(Constants.getOptionalFields(), new ArrayList<>());
        drivingLicenseOcr.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("drivingLicenseOcr", drivingLicenseOcr);

        Map<String, ArrayList<String>> drivingLicenseDetails = new HashMap<>();
        drivingLicenseDetails.put(Constants.getMandateFields(), new ArrayList<>(Arrays.asList("dl_number", "date_of_birth")));
        drivingLicenseDetails.put(Constants.getOptionalFields(), new ArrayList<>());
        drivingLicenseDetails.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("drivingLicenseDetails", drivingLicenseDetails);

        Map<String, ArrayList<String>> passportOcr = new HashMap<>();
        passportOcr.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        passportOcr.put(Constants.getOptionalFields(), new ArrayList<>());
        passportOcr.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("passportOcr", passportOcr);

        Map<String, ArrayList<String>> companyDetails = new HashMap<>();
        companyDetails.put(Constants.getMandateFields(), new ArrayList<>());
        companyDetails.put(Constants.getOptionalFields(), new ArrayList<>());
        companyDetails.put(Constants.getAnyFields(), new ArrayList<>(Arrays.asList(Constants.getCompanyName(), "cin")));
        dataSchema.put("companyDetails", companyDetails);

        Map<String, ArrayList<String>> coiOcr = new HashMap<>();
        coiOcr.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        coiOcr.put(Constants.getOptionalFields(), new ArrayList<>());
        coiOcr.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("coiOcr", coiOcr);

        Map<String, ArrayList<String>> coiVerification = new HashMap<>();
        coiVerification.put(Constants.getMandateFields(), new ArrayList<>());
        coiVerification.put(Constants.getOptionalFields(), new ArrayList<>());
        coiVerification.put(Constants.getAnyFields(), new ArrayList<>(Arrays.asList(Constants.getCompanyName(), "cin")));
        dataSchema.put("coiVerification", coiVerification);

        Map<String, ArrayList<String>> domainIdentification = new HashMap<>();
        domainIdentification.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getCompanyName())));
        domainIdentification.put(Constants.getOptionalFields(), new ArrayList<>());
        domainIdentification.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("domainIdentification", domainIdentification);

        Map<String, ArrayList<String>> rcVerification = new HashMap<>();
        rcVerification.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList("vehicle_number")));
        rcVerification.put(Constants.getOptionalFields(), new ArrayList<>());
        rcVerification.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("rcVerification", rcVerification);

        Map<String, ArrayList<String>> faceCompare = new HashMap<>();
        faceCompare.put(Constants.getMandateFields(), new ArrayList<>(Arrays.asList("url_1", "url_2")));
        faceCompare.put(Constants.getOptionalFields(), new ArrayList<>());
        faceCompare.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("faceCompare", faceCompare);

        Map<String, ArrayList<String>> faceValidation = new HashMap<>();
        faceValidation.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        faceValidation.put(Constants.getOptionalFields(), new ArrayList<>());
        faceValidation.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("faceValidation", faceValidation);

        Map<String, ArrayList<String>> panValidation = new HashMap<>();
        panValidation.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        panValidation.put(Constants.getOptionalFields(), new ArrayList<>());
        panValidation.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("panValidation", panValidation);

        Map<String, Map<String, Map<String, ArrayList<String>>>> dataSchemaMap = new HashMap<>();
        dataSchemaMap.put("dataSchema", dataSchema);
        taskConfig.put("v2", dataSchemaMap);

        Map<String, ArrayList<String>> gstOcr = new HashMap<>();
        gstOcr.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList(Constants.getDocUrl())));
        gstOcr.put(Constants.getOptionalFields(), new ArrayList<>());
        gstOcr.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("gst_ocr", gstOcr);

        Map<String, ArrayList<String>> gstVerification = new HashMap<>();
        gstVerification.put(Constants.getMandateFields(), new ArrayList<>(Collections.singletonList("gstin")));
        gstVerification.put(Constants.getOptionalFields(), new ArrayList<>());
        gstVerification.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("gst_verification", gstVerification);

        Map<String, ArrayList<String>> aadhaarVerification = new HashMap<>();
        aadhaarVerification.put(Constants.getMandateFields(), new ArrayList<>(Arrays.asList("aadhaar_number","aadhaar_name","aadhaar_consent")));
        aadhaarVerification.put(Constants.getOptionalFields(), new ArrayList<>());
        aadhaarVerification.put(Constants.getAnyFields(), new ArrayList<>());
        dataSchema.put("aadhaar_verification", aadhaarVerification);

    }
}
