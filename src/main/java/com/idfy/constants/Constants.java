package com.idfy.constants;

import java.util.*;

public class Constants {
    private static final String API_KEY = "apikey";
    private static final String APPLICATION_JSON = "application/json";
    private static final String DOC_URL = "doc_url";
    private static final String COMPANY_NAME = "company_name";
    private static final String MANDATE_FIELDS = "mandateFields";
    private static final String OPTIONAL_FIELDS = "optionalFields";
    private static final String ANY_FIELDS = "anyFields";
    private static final String BASE_URL_V2 = "https://api.idfystaging.com/v2/tasks";
    private static final List<String> AVAILABLE_TASKS = new ArrayList<>(Arrays.asList("pan_ocr", "panVerification", "aadhaarOcr", "chequeOcr", "voterOcr", "voterVerification", "drivingLicenseOcr", "drivingLicenseDetails", "passportOcr", "companyDetails", "coiOcr", "coiVerification", "domainIdentification", "rcVerification", "faceCompare", "faceValidation", "panValidation"));
    private static final Map<String, List<String>> V2_AVAILABLE_TASKS = new HashMap<>();
    private static final List<String> responseQueryArgs = new ArrayList<>();

    public static List<String> getResponseQueryArgs() {
        responseQueryArgs.add("request_id");
        responseQueryArgs.add("group_id");
        responseQueryArgs.add("task_id");
        return responseQueryArgs;
    }

    private Constants(){
        throw new IllegalStateException("Utility class");
    }

    public static String getApiKey() {
        return API_KEY;
    }

    public static String getApplicationJson() {
        return APPLICATION_JSON;
    }

    public static String getDocUrl() {
        return DOC_URL;
    }

    public static String getCompanyName() {
        return COMPANY_NAME;
    }

    public static String getMandateFields() {
        return MANDATE_FIELDS;
    }

    public static String getOptionalFields() {
        return OPTIONAL_FIELDS;
    }

    public static String getAnyFields() {
        return ANY_FIELDS;
    }

    public static String getBaseUrlV2() {
        return BASE_URL_V2;
    }

    public static Map<String, List<String>> getV2AvailableTasks() {
        V2_AVAILABLE_TASKS.put("v2", AVAILABLE_TASKS);
        return V2_AVAILABLE_TASKS;
    }
}
